var clientesObtenidos;

function getCustomers() {
  //var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers/?$filter=Country eq 'France'";
  var url = "http://services.odata.org/V4/Northwind/Northwind.svc/Customers/";
  var request = new XMLHttpRequest();
  request.onreadystatechange = function()
  {
    if (this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();

}

function procesarClientes()
{
  var JSONClientes = JSON.parse(clientesObtenidos);
  var tabla = document.getElementById('tablaClientes');
  var URLBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"
  var extension = ".png"
  for (var i = 0; i < JSONClientes.value.length; i++) {
    var nuevaFila = document.createElement("tr");
    var columnaNombre = document.createElement("td");
    columnaNombre.innerText = JSONClientes.value[i].CustomerID;
    var columnaCiudad = document.createElement("td");
    columnaCiudad.innerText = JSONClientes.value[i].City;
    var columnaPais = document.createElement("img");
    var pais = "";
    columnaPais.classList.add("flag");
    switch (JSONClientes.value[i].Country) {
      case "UK":
        pais = "United-Kingdom"
        break;
      default:
        pais =JSONClientes.value[i].Country;
    }
    //columnaPais.src = URLBandera + JSONClientes.value[i].Country + extension;
    columnaPais.src = URLBandera + pais + extension;
    nuevaFila.appendChild(columnaNombre);
    nuevaFila.appendChild(columnaCiudad);
    nuevaFila.appendChild(columnaPais);
    tabla.appendChild(nuevaFila);
    console.log();(JSONClientes.value[i].CustomerID);
  }
}
